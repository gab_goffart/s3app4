package validate

import (
	"fmt"
	"math/big"
	"bufio"
	"os"
)

func IsPrime() {
	num := &big.Int{}
	reader := bufio.NewReader(os.Stdin)
	var buf []byte

	fmt.Print("Veuillez entrer le nombre à valider : ")
	buf, _ = reader.ReadBytes('\n')

	num.UnmarshalText(buf)

	if num.ProbablyPrime(64) {
		fmt.Println("Ce nombre a de bonnes chances d'etre premier")
	} else {
		fmt.Println("Ce nombre est un nombre de Carmichael")
	}
}

func IsSafeG() {
	g := big.NewInt(0)
	p := big.NewInt(0)
	q := big.NewInt(0)
	m := big.NewInt(0)
	one := big.NewInt(1)
	pmo := big.NewInt(0)
	reader := bufio.NewReader(os.Stdin)
	var buf []byte

	fmt.Print("Veuillez entrer g : ")
	buf, _ = reader.ReadBytes('\n')
	g.UnmarshalText(buf)

	fmt.Print("Veuillez entrer p : ")
	buf, _ = reader.ReadBytes('\n')
	p.UnmarshalText(buf)

	fmt.Print("Veuillez entrer q : ")
	buf, _ = reader.ReadBytes('\n')
	q.UnmarshalText(buf)
	pmo.Sub(p, one)

	m.Exp(g, q, p)

	if m.Cmp(one) != 0 {
		fmt.Println("g n'est pas generateur dans l'espace p")
	}

	m.Mod(p, q)

	if m.Cmp(one) != 0 {
		fmt.Println("p - 1 n'est pas un facteur de q")
	}

	if !p.ProbablyPrime(64) || !q.ProbablyPrime(64) {
		fmt.Println("p ou q n'est pas premier")
	}
}

