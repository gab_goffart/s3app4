package indic

import (
	"fmt"
	"bufio"
	"os"
	"math/big"
)

func DoIndic() {
	reader := bufio.NewReader(os.Stdin)
	var buf []byte
	p := big.NewInt(0)
	q := big.NewInt(0)
	one := big.NewInt(1)
	phi := big.NewInt(0)
	pmo := big.NewInt(0)
	qmo := big.NewInt(0)

	fmt.Print("Veuillez entrer la valeur de p : ")
	buf, _ = reader.ReadBytes('\n')
	p.UnmarshalText(buf)
	pmo.Sub(p, one)

	fmt.Print("Veuillez entrer la valeur de q : ")
	buf, _ = reader.ReadBytes('\n')
	q.UnmarshalText(buf)
	qmo.Sub(q, one)

	phi.Mul(pmo, qmo)

	fmt.Println("La valeur de phi : ", phi)
}
