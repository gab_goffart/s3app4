package decrypt

import (
	"fmt"
	"bufio"
	"os"
	"io/ioutil"
	"math/big"
)

func DoDecrypt() {
	reader := bufio.NewReader(os.Stdin)
	var buf []byte
	d := big.NewInt(0)
	n := big.NewInt(0)
	m := big.NewInt(0)
	decrypted := big.NewInt(0)

	fmt.Print("Veuillez entrer la valeur de d : ")
	buf, _ = reader.ReadBytes('\n')
	d.UnmarshalText(buf)

	fmt.Print("Veuillez entrer la valeur de n : ")
	buf, _ = reader.ReadBytes('\n')
	n.UnmarshalText(buf)

	fmt.Print("Veuillez entrer le message codé : ")
	buf, _ = reader.ReadBytes('\n')
	m.UnmarshalText(buf)

	decrypted.Exp(m, d, n)

	fmt.Println("Le message decrypté : ", decrypted)

}

func DoDecryptFile() {
	var buf []byte
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Veuillez entrer la valeur de d : ")
	buf, _ = reader.ReadBytes('\n')
	intbuf := big.NewInt(0)
	intbuf.UnmarshalText(buf)
	key := intbuf.Bytes()

	fmt.Print("Veuillez entrer le nom du fichier encode : ")
	input, _, _ := reader.ReadLine()
	inputBytes, _ := ioutil.ReadFile(string(input))

	fmt.Print("Veuillez entrer le nom du fichier de sortie : ")
	output, _, _ := reader.ReadLine()
	var outputBytes []byte

	for i, char := range inputBytes {
		char ^= key[len(key) - 1 - i % len(key)]
		outputBytes = append(outputBytes, char)
	}

	ioutil.WriteFile(string(output), outputBytes, 0644)
}

