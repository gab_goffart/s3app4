package facto

import (
	"math/big"
	"fmt"
	"os"
	"bufio"
	"tools/polar"
)

func DoPmu() {
	reader := bufio.NewReader(os.Stdin)
	var buf []byte
	a := big.NewInt(2)
	n := big.NewInt(0)
	p := big.NewInt(0)
	q := big.NewInt(0)

	fmt.Print("Veuillez entrer le nombre à factoriser : ")
	buf, _ = reader.ReadBytes('\n')
	n.UnmarshalText(buf)

	p = polar.Pmo(a, a, n)
	q.Quo(n, p)

	fmt.Println("La valeur de p : ", p)
	fmt.Println("La valeur de q : ", q)
}

func DoRho() {
	reader := bufio.NewReader(os.Stdin)
	var buf []byte
	n := big.NewInt(0)

	fmt.Print("Veuillez entrer le nombre à factoriser : ")
	buf, _ = reader.ReadBytes('\n')
	n.UnmarshalText(buf)

	polar.Rho(n)
}


