package polar

import (
	"math/big"
	"fmt"
)

func Pmo(a, f, n *big.Int) *big.Int {
	one := big.NewInt(1)
	divider := big.NewInt(0)
	fac := big.NewInt(0)
	facMinusOne := big.NewInt(0)

	fac.Exp(a, f, n)

	facMinusOne.Sub(fac, one)
	divider.GCD(nil , nil, facMinusOne, n)

	if divider.Cmp(one) > 0 {
		return divider
	}

	f.Add(f, one)
	return Pmo(fac, f, n)
}

func Rho(n *big.Int) {
	one := big.NewInt(1)
	a := big.NewInt(2)
	b := big.NewInt(2)
	d := big.NewInt(0)
	r := n
	sub := big.NewInt(0)

	for {
		a = squaredPlusOne(a, n)
		b = squaredPlusOne(squaredPlusOne(b, n), n)
		sub.Sub(a, b)
		d.GCD(nil, nil, sub, n)

		if d.Cmp(n) == 0 {
			return 
		}

		if d.Cmp(one) > 0 && d.Cmp(n) < 0 {
			fmt.Printf("Facteur : %d\n", d)
			a = big.NewInt(2)
			b = big.NewInt(2)
			r.Quo(r, d)
			if r.ProbablyPrime(64) {
				fmt.Printf("Facteur : %d\n", r)
				return
			}
		}
	}
}

func squaredPlusOne(val, n *big.Int) *big.Int {
	one := big.NewInt(1)
	valSquared := &big.Int{}
	plusOne := &big.Int{}
	valSquared.Exp(val, big.NewInt(2), n)

	plusOne.Add(valSquared, one)

	return plusOne
}
