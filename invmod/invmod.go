package invmod

import (
	"fmt"
	"bufio"
	"os"
	"math/big"
)

func DoInvmod() {
	reader := bufio.NewReader(os.Stdin)
	var buf []byte
	e := big.NewInt(0)
	d := big.NewInt(0)
	phi := big.NewInt(0)

	fmt.Print("Veuillez entrer la valeur de e : ")
	buf, _ = reader.ReadBytes('\n')
	e.UnmarshalText(buf)

	fmt.Print("Veuillez entrer la valeur de phi : ")
	buf, _ = reader.ReadBytes('\n')
	phi.UnmarshalText(buf)

	d.ModInverse(e, phi)

	fmt.Println("La valeur de d : ", d)
}




