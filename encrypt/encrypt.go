package encrypt

import (
	"fmt"
	"bufio"
	"os"
	"math/big"
)

func DoEncrypt() {
	reader := bufio.NewReader(os.Stdin)
	var buf []byte
	e := big.NewInt(0)
	n := big.NewInt(0)
	m := big.NewInt(0)
	encrypted := big.NewInt(0)

	fmt.Print("Veuillez entrer la valeur de e : ")
	buf, _ = reader.ReadBytes('\n')
	e.UnmarshalText(buf)

	fmt.Print("Veuillez entrer la valeur de n : ")
	buf, _ = reader.ReadBytes('\n')
	n.UnmarshalText(buf)

	fmt.Print("Veuillez entrer le message : ")
	mes, _ := reader.ReadString('\n')
	m.SetString(mes, 62)

	encrypted.Exp(m, e, n)

	fmt.Println("Le message encrypté : ", encrypted)


}
