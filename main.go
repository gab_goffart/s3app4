package main

import (
	"os"
	"tools/facto"
	"tools/indic"
	"tools/encrypt"
	"tools/decrypt"
	"tools/invmod"
	"tools/validate"

	"github.com/urfave/cli"
)

func main() {

	app := cli.NewApp()
	app.EnableBashCompletion = true
	app.Name = "Tools - outils de déchiffrement"
	app.Usage = ""
	app.UsageText = "tools <command>"
	app.Version = "0.0.1"
	app.Commands = []cli.Command {
		{
			Name: "pmu",
			Usage: "Factorise un grand nombre a l'aide de P-1",
			Action: func (c *cli.Context) error {
				facto.DoPmu()
				return nil
			},
		},
		{
			Name: "rho",
			Usage: "Factorise un grand nombre a l'aide de Rho",
			Action: func (c *cli.Context) error {
				facto.DoRho()
				return nil
			},
		},
		{
			Name: "indic",
			Usage: "Calcul de phi(n)",
			Action: func (c *cli.Context) error {
				indic.DoIndic()
				return nil
			},
		},
		{
			Name: "invmod",
			Usage: "Calcul de l'inverse modulaire",
			Action: func (c *cli.Context) error {
				invmod.DoInvmod()
				return nil
			},
		},
		{
			Name: "encrypt",
			Usage: "Encryption de message",
			Action: func (c *cli.Context) error {
				encrypt.DoEncrypt()
				return nil
			},

		},
		{
			Name: "decrypt",
			Usage: "Décryption de message",
			Action: func (c *cli.Context) error {
				decrypt.DoDecrypt()
				return nil
			},
		},
		{
			Name: "dfile",
			Usage: "Décryption d'un fichier",
			Action: func (c *cli.Context) error {
				decrypt.DoDecryptFile()
				return nil
			},
		},
		{
			Name: "prime",
			Usage: "validation de la primalite d'un nombre",
			Action: func (c *cli.Context) error {
				validate.IsPrime()
				return nil
			},
		},
		{
			Name: "triplet",
			Usage: "validation d'un triplet q, p, g",
			Action: func (c *cli.Context) error {
				validate.IsSafeG()
				return nil
			},
		},

	}

	app.Run(os.Args)
}




